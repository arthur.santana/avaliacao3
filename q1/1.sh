#!/bin/bash

while true; do
	echo "<>menu<>"
	echo "1- Verificar existencia do usuário;"
	echo "2- Verificar se o usuário está logado na máquina;"
	echo "3- listar os arquivos da pasta home do usuário;"
	echo "4- Sair;"
	read -p "Escolha" alternativa
	
	case $alternativa in
		1)
			read -p "Nome do usuário:" nome
			id -u "$nome" >/dev/null 2>&1
			if [ $? -eq 0 ]; then
				echo "Usuário $nome existente"
			else
			        echo "Usuário $nome inexistente"
			fi
		        ;;

		2)      
		        read -p "Nome do usuário" nome
		        who | grep -w "$nome" >/dev/null 2>&1
		        if [ $? -ep 0 ]; then
			        echo "O usuário $nome se encontra logado"
			else
			        echo "O usuário $nome não se encontra logado"
			fi
		        ;;
		
		3)
		        read -p "Nome do usuário:" nome
		        ls -l /home/"$nome"
		        ;;

		4)
		        echo "sair"
		        break
		        ;;
	esac
        echo
done	
